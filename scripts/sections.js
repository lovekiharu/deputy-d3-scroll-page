let dataset, svg, datasetStatistic;
let salarySizeScale,
  salaryXScale,
  categoryColorScale,
  strengthScale,
  protestColorScale;
let simulation, nodes;
let categoryLegend, salaryLegend;

const categoriesType = ['коалиция', 'оппозиция', 'нет'];

const categoriesTypeLabels = {
  коалиция: 'Коалиция большинства',
  оппозиция: 'Оппозиция',
  нет: 'Не были членами парламента',
};
const categoriesTypeColors = ['#9a3e4e', '#4a86ab', '#cc0066'];

const protestType = ['да', 'нет', 'остались'];
const slide5Labels = {
  да: '10-ка «протестных»',
  нет: '«Непротестные»',
};
const slide6Labels = {
  да: '«Протестные» в парламенте',
  нет: '«Непротестные» депутаты',
  остались: '«Протестные» покинули парламент',
};
const slide7Labels = {
  да: 'Идут на выборы',
  нет: 'Не идут на выборы',
};
const protestTypeColor = ['#2B0504', '#D89D6A', '#4f8a8b'];
const consignments = [
  'нет',
  'Республика',
  'Биримдик',
  'Ата Мекен',
  'Бутун Кыргызстан',
  'Замандаш',
  'Бир Бол',
  'Мекеним Кыргызстан',
  'Социал-Демократы',
  'Мекенчил',
  'Кыргызстан',
];

const consignmentsColors = {
  Кыргызстан: '#96ADC8',
  Мекенчил: '#D7FFAB',
  'Социал-Демократы': '#FCFF6C',
  'Мекеним Кыргызстан': '#D89D6A',
  'Бир Бол': '#6D454C',
  Замандаш: '#2B0504',
  'Бутун Кыргызстан': '#874000',
  'Ата Мекен': '#F4442E',
  Биримдик: '#010001',
  Республика: '#4f8a8b',
  нет: '#fbd46d',
};

const colors = ['#9a3e4e', '#4a86ab', '#cc0066', '#2B0504'];

const margin = { left: 0, top: 50, bottom: 50, right: 0 };
const width = 1000 - margin.left - margin.right;
let height = 700 - margin.top - margin.bottom;

//Read Data, convert numerical categories into floats
//Create the initial visualisation

let categoriesTypeXY = {
  коалиция: [300, 150, 57382, 23.9],
  оппозиция: [700, 150, 43538, 30.5],
  нет: [400, 500, 41890, 20.9],
};
let protestTypeXY = {
  да: [300, 150, 57382, 23.9],
  нет: [700, 400, 43538, 30.5],
  остались: [300, 500, 41890, 20.9],
};
let parlamentXY = {
  да: [300, 150, 57382, 23.9],
  нет: [700, 400, 43538, 30.5],
};
let consignmentsXY = {
  Кыргызстан: [300, 0, 57382, 23.9],
  'Мекеним Кыргызстан': [500, 0, 42200, 48.3],
  Биримдик: [750, 10, 36421, 58.7],
  Республика: [300, 220, 36825, 79.5],
  'Ата Мекен': [530, 220, 37344, 55.4],
  'Бутун Кыргызстан': [300, 600, 36342, 35.0],
  Замандаш: [300, 450, 36900, 40.5],
  'Социал-Демократы': [750, 210, 41890, 50.9],
  'Бир Бол': [500, 450, 33062, 60.4],
  Мекенчил: [500, 600, 43538, 48.3],
  нет: [800, 500, 42745, 31.2],
};

let scaleMobile = 1.2;
let isMobile = false;

if (windowWidth < 768) {
  isMobile = true;
  scaleMobile = 3;
  height = 2000;

  categoriesTypeXY = {
    коалиция: [50, 550, 57382, 23.9],
    оппозиция: [800, 550, 43538, 30.5],
    нет: [400, 1100, 41890, 20.9],
  };
  protestTypeXY = {
    да: [100, 250, 57382, 23.9],
    нет: [300, 1000, 43538, 30.5],
    остались: [500, 250, 41890, 20.9],
  };
  parlamentXY = {
    да: [350, 500, 57382, 23.9],
    нет: [320, 1350, 43538, 30.5],
  };
  consignmentsXY = {
    Кыргызстан: [0, 250, 57382, 23.9],
    'Мекеним Кыргызстан': [350, 250, 42200, 48.3],
    Биримдик: [800, 250, 36825, 79.5],
    Республика: [0, 650, 37344, 55.4],
    'Ата Мекен': [350, 650, 33062, 60.4],
    'Социал-Демократы': [800, 650, 41890, 50.9],
    Замандаш: [0, 950, 36900, 40.5],
    'Бир Бол': [300, 950, 42745, 31.2],
    'Бутун Кыргызстан': [570, 950, 36342, 35.0],
    Мекенчил: [850, 1000, 43538, 48.3],
    нет: [350, 1450, 36421, 58.7],
  };
}

d3.csv('../deputies.csv', function (d) {
  return {
    name: d.name,
    image: d.image,
    size: parseInt(d.size) + 1,
    type2: d.type2,
    size2: parseInt(d.size2) + 1,
    size3: parseInt(d.size3) + 1,
    type3: d.type3,
    size4: parseInt(d.size4) + 1,
    size5: parseInt(d.size5) + 1,
    protest5: d.protest5,
    size6: parseInt(d.size6) + 1,
    protest6: d.protest6,
    isOut6: d.isOut6,
    size7: parseInt(d.size7) + 1,
    toParlament7: d.toParlament7,
    size8: parseInt(d.size8) + 1,
    consignment8: d.consignment8,
  };
}).then((data) => {
  dataset = data;
  createScales();
  setTimeout(drawInitial(), 100);
});

// All the initial elements should be create in the drawInitial function
// As they are required, their attributes can be modified
// They can be shown or hidden using their 'opacity' attribute
// Each element should also have an associated class name for easy reference
function createScales() {
  salarySizeScale = d3.scaleLinear(
    d3.extent(dataset, (d) => d.size),
    [5, 15]
  );
  categoryColorScale = d3.scaleOrdinal(categoriesType, categoriesTypeColors);
  protestColorScale = d3.scaleOrdinal(protestType, protestTypeColor);

  strengthScale = d3
    .scalePow()
    .exponent(3)
    .range([3, 10])
    .domain([
      5,
      d3.max(dataset, function (d) {
        return d.size * scaleMobile;
      }),
    ]);
}

function drawInitial() {
  let svg = d3
    .select('#vis')
    .append('svg')
    .attr('viewBox', `0 0 ${width} ${height}`)
    .attr('opacity', 1);

  simulation = d3.forceSimulation(dataset);
  nodes = svg.selectAll(null).data(dataset);

  var nodeEnter = nodes
    .enter()
    .append('svg:g')
    .attr('class', 'node')
    .attr('transform', function (d) {
      return 'translate(' + d.x + ',' + d.y + ')';
    });

  // Append a circle
  nodeEnter
    .append('svg:circle')
    .attr('r', (d) => salarySizeScale(d.size) * scaleMobile)
    .attr('fill', colors[0]);

  // Append images
  var images = nodeEnter
    .append('svg:image')
    .attr('xlink:href', function (d) {
      return d.image ? './images/' + d.image : null;
    })
    .attr('x', function (d) {
      return salarySizeScale(d.size) * scaleMobile * -1;
    })
    .attr('y', function (d) {
      return salarySizeScale(d.size) * scaleMobile * -1;
    })
    .attr('opacity', function (d) {
      return d.image ? 1 : 0;
    })
    .attr('height', (d) => salarySizeScale(d.size) * 2 * scaleMobile)
    .attr('width', (d) => salarySizeScale(d.size) * 2 * scaleMobile);

  simulation.on('tick', function () {
    nodeEnter.attr('transform', function (d) {
      return 'translate(' + d.x + ',' + d.y + ')';
    });
  });
  // Selection of all the circles
  simulation
    .force('charge', d3.forceManyBody().strength(0.8))
    .force('forceX', d3.forceX(500))
    .force('forceY', d3.forceY(500))
    .force(
      'x',
      d3.forceX(width / 2).strength(function (d) {
        return strengthScale(d.size);
      })
    )
    .force(
      'y',
      d3.forceY(height / 2).strength(function (d) {
        return strengthScale(d.size);
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(1)
        .radius(function (d) {
          return salarySizeScale(d.size) * scaleMobile + 4;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();

  // Add mouseover and mouseout events for all circles
  // Changes opacity and adds border
  svg.selectAll('g.node').on('mouseover', mouseOver).on('mouseout', mouseOut);

  function mouseOver(d, i) {
    d3.select(this)
      .transition('mouseover')
      .duration(100)
      .attr('opacity', 1)
      .attr('stroke-width', 2)
      .attr('stroke', 'black');

    d3.select('#tooltip')
      .style('left', d3.event.clientX - 30 + 'px')
      .style('top', d3.event.clientY + 30 + 'px')
      .style('display', 'inline-block')
      .html(`<strong>${d.name}</strong>`);
  }

  function mouseOut(d, i) {
    d3.select('#tooltip').style('display', 'none');

    d3.select(this)
      .transition('mouseout')
      .duration(100)
      .attr('opacity', 0.8)
      .attr('stroke-width', 0);
  }
}

//First draw function
function drawFirst() {
  simulation.stop();
  let svg = d3.select('#vis').select('svg');

  svg.selectAll('text').remove();

  svg
    .selectAll('circle')
    .transition()
    .duration(500)
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size2) * scaleMobile)
    .attr('fill', (d) => colors[0]);

  simulation
    .force('charge', d3.forceManyBody().strength(0.8))
    .force('forceX', d3.forceX(500))
    .force('forceY', d3.forceY(500))
    .force(
      'x',
      d3.forceX(width / 2).strength(function (d) {
        return strengthScale(d.size);
      })
    )
    .force(
      'y',
      d3.forceY(height / 2).strength(function (d) {
        return strengthScale(d.size);
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(0.8)
        .radius(function (d) {
          return salarySizeScale(d.size) * scaleMobile + 4;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

function drawSecond() {
  simulation.stop();
  let svg = d3.select('#vis').select('svg');

  svg.selectAll('text').remove();
  for (const [key, value] of Object.entries(categoriesTypeXY)) {
    svg
      .append('text')
      .attr('class', 'nodetext')
      .attr('x', function () {
        if (value[0] === 300) {
          return 350;
        }
        if (value[0] === 700) {
          return 650;
        }
        if (value[0] === 400) {
          return isMobile ? 350 : 430;
        }
        if (value[0] === 50) {
          return 120;
        }
        if (value[0] === 800) {
          return 700;
        }
        return value[0];
      })
      .attr('y', function () {
        if (value[1] === 150) {
          return 90;
        }
        if (value[1] === 500) {
          return 350;
        }
        if (value[1] === 550) {
          return 210;
        }
        if (value[1] === 1100) {
          return 980;
        }
        return value[1];
      })
      .attr('fill', '#000')
      .attr('font-size', isMobile ? '30' : '12')
      .text(function () {
        return categoriesTypeLabels[key];
      });
  }

  svg
    .selectAll('circle')
    .transition()
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size2) * scaleMobile)
    .attr('fill', (d) => categoryColorScale(d.type2));

  simulation
    .force('charge', d3.forceManyBody().strength([5]))
    .force(
      'x',
      d3.forceX().x(function (d) {
        return categoriesTypeXY[d.type2][0] + 100;
      })
    )
    .force(
      'y',
      d3.forceY().y(function (d) {
        return categoriesTypeXY[d.type2][1] - 100;
      })
    )
    .force(
      'cX',
      d3.forceX().x(function (d) {
        return categoriesTypeXY[d.type2][0] + 100;
      })
    )
    .force(
      'cY',
      d3.forceY().y(function (d) {
        return categoriesTypeXY[d.type2][1] - 100;
      })
    )
    .force(
      'collision',
      d3.forceCollide().radius(function (d) {
        return d.size2;
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(1)
        .radius(function (d) {
          return salarySizeScale(d.size2) * scaleMobile + 2;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

function drawThird() {
  simulation.stop();
  let svg = d3.select('#vis').select('svg');
  svg.selectAll('text').remove();
  for (const [key, value] of Object.entries(categoriesTypeXY)) {
    svg
      .append('text')
      .attr('class', 'nodetext')
      .attr('x', function () {
        if (value[0] === 300) {
          return 350;
        }
        if (value[0] === 700) {
          return 650;
        }
        if (value[0] === 400) {
          return isMobile ? 350 : 430;
        }
        if (value[0] === 50) {
          return 120;
        }
        if (value[0] === 800) {
          return 700;
        }
        return value[0];
      })
      .attr('y', function () {
        if (categoriesTypeLabels[key] === 'Оппозиция') {
          return isMobile ? 260 : 110;
        }
        if (value[1] === 150) {
          return 80;
        }
        if (value[1] === 500) {
          return 350;
        }
        if (value[1] === 550) {
          return 160;
        }
        if (value[1] === 1100) {
          return 1000;
        }
        return value[1];
      })
      .attr('fill', '#000')
      .attr('font-size', isMobile ? '30' : '12')
      .text(function () {
        return categoriesTypeLabels[key];
      });
  }
  svg
    .selectAll('circle')
    .transition()
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size3) * scaleMobile)
    .attr('fill', (d) => categoryColorScale(d.type3));

  simulation
    .force('charge', d3.forceManyBody().strength([5]))
    .force(
      'x',
      d3.forceX().x(function (d) {
        return categoriesTypeXY[d.type3][0] + 100;
      })
    )
    .force(
      'cX',
      d3.forceX().x(function (d) {
        return categoriesTypeXY[d.type3][0] + 100;
      })
    )
    .force(
      'y',
      d3.forceY().y(function (d) {
        return categoriesTypeXY[d.type3][1] - 100;
      })
    )
    .force(
      'cY',
      d3.forceY().y(function (d) {
        return categoriesTypeXY[d.type3][1] - 100;
      })
    )
    .force(
      'collision',
      d3.forceCollide().radius(function (d) {
        return d.size3;
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(1)
        .radius(function (d) {
          return salarySizeScale(d.size3) * scaleMobile + 2;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

function drawFour() {
  simulation.stop();
  let svg = d3.select('#vis').select('svg');

  svg.selectAll('text').remove();
  svg
    .selectAll('circle')
    .transition()
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size4) * scaleMobile)
    .attr('fill', colors[0]);

  simulation
    .force('forceX', d3.forceX(500))
    .force('forceY', d3.forceY(500))
    .force(
      'x',
      d3.forceX(width / 2).strength(function (d) {
        return strengthScale(d.size4);
      })
    )
    .force(
      'y',
      d3.forceY(height / 2).strength(function (d) {
        return strengthScale(d.size4);
      })
    )
    .force('charge', d3.forceManyBody().strength(0.8))
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(0.8)
        .radius(function (d) {
          return salarySizeScale(d.size4) * scaleMobile + 4;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

function drawFive() {
  simulation.stop();
  let svg = d3.select('#vis').select('svg');

  svg.selectAll('text').remove();

  for (const [key, value] of Object.entries(protestTypeXY)) {
    svg
      .append('text')
      .attr('class', 'nodetext')
      .attr('x', function () {
        if (!isMobile) {
          return value[0] === 300 ? 390 : 650;
        } else {
          return value[0] === 300 ? 360 : 460;
        }
      })
      .attr('y', function () {
        if (!isMobile) {
          return value[1] === 150 ? 120 : 200;
        } else {
          return value[1] === 1000 ? 1150 : 200;
        }
      })
      .attr('fill', '#000')
      .attr('font-size', isMobile ? '30' : '12')
      .text(function () {
        return slide5Labels[key];
      });
  }

  svg
    .selectAll('circle')
    .transition()
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size5) * scaleMobile)
    .attr('fill', (d) => protestColorScale(d.protest5));

  simulation
    .force('charge', d3.forceManyBody().strength([5]))
    .force(
      'x',
      d3.forceX().x(function (d) {
        return protestTypeXY[d.protest5][0] + 100;
      })
    )
    .force(
      'cX',
      d3.forceX().x(function (d) {
        return protestTypeXY[d.protest5][0] + 100;
      })
    )
    .force(
      'y',
      d3.forceY().y(function (d) {
        return protestTypeXY[d.protest5][1] - 100;
      })
    )
    .force(
      'cY',
      d3.forceY().y(function (d) {
        return protestTypeXY[d.protest5][1] - 100;
      })
    )
    .force(
      'collision',
      d3.forceCollide().radius(function (d) {
        return d.size5;
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(1)
        .radius(function (d) {
          return salarySizeScale(d.size5) * scaleMobile + 2;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

function drawSix() {
  simulation.stop();
  let svg = d3.select('#vis').select('svg');

  svg.selectAll('text').remove();
  for (const [key, value] of Object.entries(protestTypeXY)) {
    svg
      .append('text')
      .attr('class', 'nodetext')
      .attr('x', function () {
        if (value[0] === 100) {
          return 60;
        }
        if (value[0] === 300) {
          return isMobile ? 290 : 350;
        }
        if (value[0] === 700) {
          return 630;
        }
        if (value[0] === 300) {
          return 370;
        }
        if (value[0] === 500) {
          return 410;
        }
        return value[0];
      })
      .attr('y', function () {
        if (value[1] === 150) {
          return 150;
        }
        if (value[1] === 250) {
          return value[0] === 500 ? 410 : 160;
        } 
        if (value[1] === 400) {
          return 200;
        }
        if (value[1] === 500) {
          return 370;
        }
        if (value[1] === 1000) {
          return 1150;
        }
        return value[1];
      })
      .attr('fill', '#000')
      .attr('font-size', isMobile ? '30' : '12')
      .text(function () {
        return slide6Labels[key];
      });
  }

  svg
    .selectAll('circle')
    .transition()
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size6) * scaleMobile)
    .attr('fill', (d) => {
      let type =
        d.protest6 === 'да' && d.isOut6 === 'да' ? 'остались' : d.protest6;
      return protestColorScale(type);
    });

  simulation
    .force('charge', d3.forceManyBody().strength([5]))
    .force(
      'x',
      d3.forceX().x(function (d) {
        let type =
          d.protest6 === 'да' && d.isOut6 === 'да' ? 'остались' : d.protest6;
        return protestTypeXY[type][0] + 100;
      })
    )
    .force(
      'cX',
      d3.forceX().x(function (d) {
        let type =
          d.protest6 === 'да' && d.isOut6 === 'да' ? 'остались' : d.protest6;
        return protestTypeXY[type][0] + 100;
      })
    )
    .force(
      'y',
      d3.forceY().y(function (d) {
        let type =
          d.protest6 === 'да' && d.isOut6 === 'да' ? 'остались' : d.protest6;
        return protestTypeXY[type][1] - 100;
      })
    )
    .force(
      'cY',
      d3.forceY().y(function (d) {
        let type =
          d.protest6 === 'да' && d.isOut6 === 'да' ? 'остались' : d.protest6;
        return protestTypeXY[type][1] - 100;
      })
    )
    .force(
      'collision',
      d3.forceCollide().radius(function (d) {
        return d.size6;
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(1)
        .radius(function (d) {
          return salarySizeScale(d.size6) * scaleMobile + 2;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

function drawSeven() {
  let svg = d3.select('#vis').select('svg');

  svg.selectAll('text').remove();
  for (const [key, value] of Object.entries(slide7Labels)) {
    svg
      .append('text')
      .attr('class', 'nodetext')
      .attr('x', function () {
        if (parlamentXY[key][0] === 300) {
          return 380;
        }
        if (parlamentXY[key][0] === 700) {
          return 640;
        }
        if (parlamentXY[key][0] === 350 || parlamentXY[key][0] === 320) {
          return 360;
        }
      })
      .attr('y', function () {
        if (parlamentXY[key][1] === 150) {
          return 60;
        }
        if (parlamentXY[key][1] === 400) {
          return 240;
        }
        if (parlamentXY[key][1] === 500) {
          return 120;
        }
        if (parlamentXY[key][1] === 1350) {
          return 1250;
        }
      })
      .attr('fill', '#000')
      .attr('font-size', isMobile ? '30' : '12')
      .text(function () {
        return value;
      });
  }

  svg
    .selectAll('circle')
    .transition()
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size7) * scaleMobile)
    .attr('fill', (d) => {
      let type = d.toParlament7 === 'идет' ? 'да' : d.toParlament7;
      return protestColorScale(type);
    });

  simulation
    .force('charge', d3.forceManyBody().strength([5]))
    .force(
      'x',
      d3.forceX().x(function (d) {
        let type = d.toParlament7 === 'идет' ? 'да' : d.toParlament7;
        return parlamentXY[type][0] + 100;
      })
    )
    .force(
      'cX',
      d3.forceX().x(function (d) {
        let type = d.toParlament7 === 'идет' ? 'да' : d.toParlament7;
        return parlamentXY[type][0] + 100;
      })
    )
    .force(
      'y',
      d3.forceY().y(function (d) {
        let type = d.toParlament7 === 'идет' ? 'да' : d.toParlament7;
        return parlamentXY[type][1] - 100;
      })
    )
    .force(
      'cY',
      d3.forceY().y(function (d) {
        let type = d.toParlament7 === 'идет' ? 'да' : d.toParlament7;
        return parlamentXY[type][1] - 100;
      })
    )
    .force(
      'collision',
      d3.forceCollide().radius(function (d) {
        return d.size7;
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(1)
        .radius(function (d) {
          return salarySizeScale(d.size7) * scaleMobile + 2;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

function drawEighth() {
  let svg = d3.select('#vis').select('svg');

  svg.selectAll('text').remove();

  for (const [key, value] of Object.entries(consignmentsXY)) {
    svg
      .append('text')
      .attr('class', 'nodetext')
      .attr('x', function () {
        switch (key) {
          case 'Кыргызстан':
            return !isMobile ? 400 : 150;
          case 'Мекеним Кыргызстан':
            return !isMobile ? 510 : 350;
          case 'Биримдик':
            return !isMobile ? 720 : 700;
          case 'Республика':
            return !isMobile ? 400 : 150;
          case 'Ата Мекен':
            return !isMobile ? 560 : 400;
          case 'Бутун Кыргызстан':
            return !isMobile ? 390 : 490;
          case 'Замандаш':
            return !isMobile ? 410 : 160;
          case 'Бир Бол':
            return !isMobile ? 560 : 350;
          case 'Социал-Демократы':
            return !isMobile ? 690 : 650;
          case 'нет':
            return !isMobile ? 720 : 380;
          case 'Мекенчил':
            return !isMobile ? 550 : 740;
        }
      })
      .attr('y', function () {
        switch (key) {
          case 'Кыргызстан':
            return !isMobile ? 40 : 135;
          case 'Мекеним Кыргызстан':
            return !isMobile ? 40 : 140;
          case 'Биримдик':
            return !isMobile ? 30 : 100; 
          case 'Республика':
            return !isMobile ? 190 : 430;
          case 'Ата Мекен':
            return !isMobile ? 200 : 440;
          case 'Бутун Кыргызстан':
            return !isMobile ? 460 : 660;
          case 'Замандаш':
            return !isMobile ? 350 : 665;
          case 'Бир Бол':
            return !isMobile ? 350 : 660;
          case 'Социал-Демократы':
            return !isMobile ? 200 : 455;
          case 'нет':
            return !isMobile ? 320 : 1320;
          case 'Мекенчил':
            return !isMobile ? 460 : 710;
        }
      })
      .attr('fill', '#000')
      .attr('font-size', isMobile ? '30' : '12')
      .text(function () {
        return key === 'нет' ? 'Не идут на выборы' : key;
      });
  }

  svg
    .selectAll('circle')
    .transition()
    .delay((d, i) => i * 10)
    .attr('r', (d) => salarySizeScale(d.size8) * scaleMobile)
    .attr('fill', (d) => {
      return consignmentsColors[d.consignment8];
    });

  simulation
    .force('charge', d3.forceManyBody().strength([5]))
    .force(
      'x',
      d3.forceX().x(function (d) {
        return consignmentsXY[d.consignment8][0] + 100;
      })
    )
    .force(
      'cX',
      d3.forceX().x(function (d) {
        return consignmentsXY[d.consignment8][0] + 100;
      })
    )
    .force(
      'y',
      d3.forceY().y(function (d) {
        return consignmentsXY[d.consignment8][1] - 100;
      })
    )
    .force(
      'cY',
      d3.forceY().y(function (d) {
        return consignmentsXY[d.consignment8][1] - 100;
      })
    )
    .force(
      'collision',
      d3.forceCollide().radius(function (d) {
        return d.size3;
      })
    )
    .force(
      'collide',
      d3
        .forceCollide()
        .strength(1)
        .radius(function (d) {
          return salarySizeScale(d.size3) * scaleMobile + 2;
        })
        .iterations(3)
    )
    .alpha(1)
    .alphaDecay(0.1)
    .restart();
}

let activationFunctions = [
  drawFirst,
  drawSecond,
  drawSecond,
  drawThird,
  drawThird,
  drawFour,
  drawFive,
  drawSix,
  drawSeven,
  drawEighth,
];

//All the scrolling function
//Will draw a new graph based on the index provided by the scroll

let scroll = scroller().container(d3.select('#graphic'));
scroll();

let lastIndex,
  activeIndex = 0;

scroll.on('active', function (index) {
  d3.selectAll('.step')
    .transition()
    .duration(500)
    .style('opacity', function (d, i) {
      return i === index ? 1 : isMobile ? 0 : 0.2;
    });

  activeIndex = index;
  let sign = activeIndex - lastIndex < 0 ? -1 : 1;
  let scrolledSections = d3.range(lastIndex + sign, activeIndex + sign, sign);
  scrolledSections.forEach((i) => {
    if (activationFunctions[i]) {
      activationFunctions[i]();
    }
  });
  lastIndex = activeIndex;
});

let isChange = false;
window.addEventListener('scroll', function () {
  var el = $('#vis').find('svg');
  topOfFooter = $('#section-end').position().top;

  scrollDistanceFromTopOfDoc = $(document).scrollTop() + (isMobile ? 490 : 700);
  scrollDistanceFromTopOfFooter = scrollDistanceFromTopOfDoc - topOfFooter;

  if (
    scrollDistanceFromTopOfDoc > topOfFooter &&
    scrollDistanceFromTopOfFooter > -50 &&
    !isChange
  ) {
    el.css({ position: 'relative', top: isMobile ? '260px' : 0 });
    isChange = true;
  } else if (scrollDistanceFromTopOfFooter < -50) {
    el.css({ position: 'absolute', top: 0  });
    isChange = false;
  }
});

d3.csv('../deputiesStat.csv', function (d) {
  return {
    name: d.name,
    fraction: d.fraction,
    votedFor: d.votedFor,
    votedAgainst: d.votedAgainst,
    lost: d.lost,
    abstained: d.abstained,
  };
}).then((data) => {
  datasetStatistic = data;
  initSelect();
});

function initSelect() {
  var selects = document.getElementsByClassName('autocomplete');

  [...selects].map((select) => {
    for (var i = 0; i < datasetStatistic.length; i++) {
      var opt = datasetStatistic[i];
      var el = document.createElement('option');
      el.textContent = opt.name;
      el.value = opt.name;
      select.appendChild(el);
    }

    $(select)
      .select2({
        placeholder: 'Выбрать депутата',
      })
      .on('select2:select', function (e) {
        var data = e.params.data;
        var deputy = datasetStatistic.find((item) => item.name === data.id);
        $('#' + select.getAttribute('id') + 'Out').html(
          `<p><b>Фракция: </b>${deputy.fraction}</p>` +
            `<p><b>За, %: </b>${deputy.votedFor}</p>` +
            `<p><b>Против, %: </b>${deputy.votedAgainst}</p>` +
            `<p><b>Прогулял(а), %: </b>${deputy.lost}</p>` +
            `<p><b>Воздержались, %: </b>${deputy.abstained}</p>`
        );
      });
  });
}
